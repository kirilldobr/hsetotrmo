

import Foundation
import Alamofire
import SwiftyJSON

class BackendFacade {
    
    struct CalculationResult {
        let sum : String
        let rate : String
    }
    
    private init() { }
    
    static var url: String {
        get {
            let u = "http://192.168.1.19"
            return u
        }
        
    }
    
    static private func request(_ suburl : String, _ completion: @escaping (JSON?)->()) {
        request(suburl, parameters: nil, completion)
    }
    
    static private func request(_ suburl : String, parameters: Parameters?, _ completion: @escaping (JSON?)->()) {
        
        var request = URLRequest(url: URL(string: "\(url)/\(suburl)")!)
        request.httpMethod = HTTPMethod.get.rawValue
        
        if parameters != nil {
            let pjson = try! JSON(parameters!).rawData()
            request.httpMethod = HTTPMethod.post.rawValue
            request.httpBody = pjson
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        
        Alamofire.request(request).responseData { (response) in
            
            guard let data = response.data else {
                completion(nil)
                print("\(suburl) unreachable")
                return
            }
            
            guard let json = try? JSON(data: data) else {
                completion(nil)
                print("can't parse json")
                return
            }
            
            completion(json)
        }
    }
    
    static func getRegions(completion: @escaping ([Region]) -> () ) {
        request("regions") { (jsonOpt) in
            guard let json = jsonOpt else {
                completion([])
                return
            }
            
            guard let regs = json["regions"].array  else  {
                completion([])
                print("regions bad response")
                return
            }
            completion(
                regs.map({ Region(id: $0["id"].stringValue,
                                  name: $0["name"].stringValue,
                                  number: $0["number"].stringValue,
                                  availableYears: $0["years"].arrayValue.map({ $0.stringValue })) })
            )
        }
    }
    
    static func getVehicleCategories(completion: @escaping ([VehicleCategory]) -> () ) {
        request("cats") { (jsonOpt) in
            guard let json = jsonOpt else {
                completion([])
                return
            }
            
            guard let cats = json["cats"].array else {
                print("categories bad response")
                completion([])
                return
            }
            completion(
                cats.map({ VehicleCategory(name: $0["name"].stringValue, key: $0["key"].stringValue) })
            )
        }
    }
    
    static func getCalculation(year: String,
                               region: String,
                               category: String,
                               power: String,
                               months: String,
                               completion: @escaping (CalculationResult?)->()) {
        request("calc?y=\(year)&r=\(region)&c=\(category)&p=\(power)&m=\(months)") { (jsonOpt) in
            guard let json = jsonOpt else {
                completion(nil)
                return
            }
            
            guard let resultSum = json["result"].int else {
                print("resultSum bad response")
                completion(nil)
                return
            }
            
            guard let resultRate = json["rate"].int else {
                print("resultRate bad response")
                completion(nil)
                return
            }
            
            completion(CalculationResult(sum: "\(resultSum)", rate: "\(resultRate)"))
        }
    }
    
    static func benefits(regid:String, year:String, completion: @escaping ([Benefit]?)->()) {
        request("benefits.json?r=\(regid)&y=\(year)") { (json) in
            let res = json?.array?.map({ benefitJson -> Benefit in
                let strings = benefitJson.arrayValue.map({ $0.stringValue })
                
                return Benefit(peopleCategory: strings[0],
                               law: strings[1],
                               documentsRequired: strings[2],
                               value: Int(strings[3])!,
                               units: BenefitUnits(strings[4]),
                               howToGet: strings[5],
                               FLULIP: strings[6])
            })
            
            completion(res)
        }
    }
    
    /// Return array of titles and fields for chosing cars' brand and model
    private static func fieldsForCars(carIdField : JSON) -> [AnyObject] {
        
        var items = [AnyObject]()
        
        if (carIdField["title"].array?.count ?? 0) >= 2 {
            
            let variants = carIdField["variants"].dictionaryValue.sorted(by: { $0.key < $1.key })
            
            
            
            let brands = variants.map { PickerVariant($0.key) }
            let models = variants.map({
                $0.value.arrayValue.map({
                    PickerVariant(($0.dictionaryValue["model"]?.stringValue)!,
                                  ($0.dictionaryValue["id"]?.stringValue)!)
                })
            })
            
            items.append(TitleItem(carIdField["title"].arrayValue[0].stringValue))
            items.append(PickerItem(fieldName: carIdField["name"].arrayValue[0].stringValue,
                                    variants: brands))
            
            items.append(TitleItem(carIdField["title"].arrayValue[1].stringValue))
            items.append(PickerItem(fieldName: carIdField["name"].arrayValue[1].stringValue,
                                    variants: models[0]))
            
            (items[1] as! PickerItem).otherPicker = items.last as? PickerItem
            (items[1] as! PickerItem).otherPickerVariants = models
        }
        
        return items
    }
    
    static func getFields(_ postFields : [TextInputItem]?, completion: @escaping ([AnyObject]?)->()) {
        
        var parameters : Parameters? = nil
        
        if postFields != nil {
            
            var fields = [Any]()
            
            postFields!.forEach { (item) in
                fields.append(["name":item.fieldName, "value":item.value])
            }
            
            parameters = ["fields": fields]
            
            print("Sending fields: \(fields)\n\n\n")
            
            //url = "http://127.0.0.1:5555"
        }
        
        request("calc2", parameters: parameters) { (jsonOpt) in
            guard let json = jsonOpt else {
                completion(nil)
                return
            }
            
            guard let fields = json["fields"].array else {
                print("fields bad response")
                completion(nil)
                return
            }
            
            var items = [AnyObject]()
            
            fields.forEach({ (field) in
                let name = field["name"].stringValue
                
                guard field["name"].array == nil else {
                    
                    if let _ = json["result"].string,
                    let _ = json["rate"].string {} else {
                        items.append(TitleItem("Моя тачка дорогая!", checkboxValue: false))
                    }
                    
                    items.append(contentsOf: self.fieldsForCars(carIdField: field))
                    
                    return
                }
                
                let title = field["title"].stringValue
                let variants = field["variants"].array?.sorted().map({ (variantJson) -> PickerVariant in
                    return PickerVariant(variantJson["text"].stringValue, variantJson["value"].stringValue)
                })
                let value = field["value"].string
                
                let fieldtype = field["fieldtype"].string
                let fieldIsInfo = fieldtype == "info"
                
                if fieldIsInfo {
                    if let d = value {
                        items.append(ResultItem(title: title, desc: value ?? ""))
                    }
                } else {
                    
                    if name == "months" {
                        items.append(TitleItem("Льгота"))
                        items.append(BenefitSelectItem())
                    }
                    
                    items.append(TitleItem(title))
                    
                    
                    if let variants = variants {
                        if name == "region",
                            let myRegion = myArea,
                            let min = variants.sorted(by: { (s1, s2) -> Bool in
                                levenshtein(aStr: s1.text, bStr: myRegion) < levenshtein(aStr: s2.text, bStr: myRegion)
                            }).first,
                            let index = variants.firstIndex(of: min) {
                            items.append(PickerItem(fieldName: name, variants: variants, initialIndex: index))
                        } else {
                            items.append(PickerItem(fieldName: name, variants: variants))
                        }
                    } else {
                        items.append(TextInputItem(fieldName: name, textType: TextType.numbers, defaultValue: value))
                    }
                    
                    
                    //                    if variants != nil {
                    //                        items.append(PickerItem(fieldName: name, variants: variants!))
                    //                    } else {
                    //                        items.append(TextInputItem(fieldName: name, textType: TextType.numbers, defaultValue: value))
                    //                    }
                }
            })
            
            
            
            if let resultSum = Decimal(string: json["result"].string ?? ""),
                let resultRate = Decimal(string: json["rate"].string ?? "") {
                //                    let calcRes = CalculationResult(sum: resultSum, rate: resultRate)
                
                // TODO add resultCell item
                
                let resultItem = Result(result: "Результат",
                                        sum: resultSum,
                                        rate: resultRate,
                                        desc: "", openPressed: {
                                            let vc = HintViewController()
                                            vc.text = formulaText
                                            UIApplication.shared.keyWindow?.rootViewController?.present(vc, animated: true, completion: nil)});
                
                items.insert(resultItem, at: 0)
            }
            
            print(items)
            completion(items)
        }
    }
}


let formulaText = """
Расчет произведен по формуле: Сумма налога (руб.) = налоговая база (мощность двигателя в лошадиных силах - в отношении транспортных средств,имеющих двигатели: паспортная статистическая тяга реактивного двигателя (суммарная паспортная тяга всех реактивных двигателей) воздушного транспортного средства на взлетном режиме в земных услоовиях в килограммах силы - в отношении воздушных транспортных средств, для которых определяется тяга реактивного двигателя: валовая вместимость в регистровых тоннах - в отношении водных несамоходных (буксируемых) средств, для которых определяется валовая вместимость) * ставка (руб.) *    (количество полных месяцев владения / 12 месяцев).
"""
