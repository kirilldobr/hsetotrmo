
import UIKit
import EasyPeasy

class HintViewController: UIViewController {
	
	private let label = UITextView()
	
	var text : String = ""

	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		
		modalPresentationStyle = .overCurrentContext
		providesPresentationContextTransitionStyle = true
		definesPresentationContext = true
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()

		let blur = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
		view.addSubview(blur)
		blur.easy.layout(Edges())
		
		let labelBg = UIView()
		labelBg.addSubview(label)
		label.easy.layout(Edges(16))
		
		label.delegate = self
		label.text = text
		label.font = UIFont.systemFont(ofSize: UIFont.labelFontSize)
		label.layoutIfNeeded()
		label.isEditable = false
		
		labelBg.layer.cornerRadius = 16
		labelBg.layer.masksToBounds = true
		labelBg.backgroundColor = UIColor.white
		
		let btn = UIButton(type: .custom)
		view.addSubview(btn)
		btn.setTitle("Закрыть", for: .normal)
		btn.setTitleColor(.black, for: .normal)
		btn.backgroundColor = .white
		btn.layer.cornerRadius = 16
		
		btn.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
		
		
		
		let centerYView = UIView()
		centerYView.backgroundColor = .clear
		
		centerYView.addSubview(labelBg)
		view.addSubview(centerYView)
		
		
		labelBg.translatesAutoresizingMaskIntoConstraints = false
		centerYView.translatesAutoresizingMaskIntoConstraints = false
		
		
		label.easy.layout(Edges(16))
		
		labelBg.easy.layout(
			CenterY(),
			Bottom(>=16).with(.custom(999)),
			Top(>=16).with(.medium),
			Left(16), Right(16))
		
		centerYView.easy.layout(
			Top().to(view.safeAreaLayoutGuide, .top),
			Bottom().to(btn, .top),
			Left(), Right())
		
		btn.easy.layout(
			Bottom(20).to(view.safeAreaLayoutGuide, .bottom),
			Left(16),
			Right(16),
			Height(60))
		
		view.backgroundColor = .clear
    }
	
	@objc func didTapClose() {
		dismiss(animated: true, completion: nil)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		label.flashScrollIndicators()
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		self .textViewDidChange(label)
	}
}

extension HintViewController: UITextViewDelegate {
	func textViewDidChange(_ textView: UITextView) {
		
		var size = view.bounds.size
		size.width -= 16 * 2
		size.height = 100000
		
		let height = self.label.sizeThatFits(size).height
		let h = min(max(30, height), view.bounds.height - 200)
		label.easy.layout(Height(h))
	}
	
}
