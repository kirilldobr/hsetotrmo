
import Foundation

enum BenefitUnits : String
{
	case percent = "%", rubles = "руб.", unknown = ""
	
	init(_ str : String) {
		switch str {
		case "%":
			self = .percent
		case "руб.":
			self = .rubles
		default:
			self = .unknown
		}
	}
}

struct Benefit
{
    
    init(peopleCategory: String,
         law: String,
         documentsRequired: String,
         value: Int,
         units: BenefitUnits,
         howToGet: String,
         FLULIP: String) {
        self.peopleCategory = peopleCategory
        self.law = law
        self.documentsRequired = documentsRequired
        self.value = value
        self.units = units
        self.howToGet = howToGet
        self.FLULIP = FLULIP
    }
    
    init(peopleCategory: String) {
        self.peopleCategory = peopleCategory
        self.law = nil
        self.documentsRequired = nil
        self.value = nil
        self.units = nil
        self.howToGet = nil
        self.FLULIP = nil
    }
    
    let peopleCategory : String //Категория налогоплательщиков, для которых установлена льгота",
    let law : String? //Соответствующая статья (пункт) закона субъекта Российской Федерации",
    let documentsRequired : String? //Основания предоставления льготы",
    let value : Int? //Размер",
    let units : BenefitUnits? //Ед. изм.",
    let howToGet : String? //Условия предоставления льготы",
    //	var FL : Bool {  }
    //	var UL : Bool //ЮЛ
    //	var IP : Bool //ИП
    let FLULIP : String?
}
