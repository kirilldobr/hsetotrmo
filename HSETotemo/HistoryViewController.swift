

import UIKit
import EasyPeasy

class HistoryViewController: UIViewController {
    
    var _results: [CalcResult]? = Array(CloudStorage.get().values)
    
    var results: [CalcResult] {
        get {
            if let r = _results {
                navigationItem.rightBarButtonItem = r.count > 0 ? history : nil
                return r
            } else {
                _results = Array(CloudStorage.get().values)
                navigationItem.rightBarButtonItem = _results!.count > 0 ? history : nil
                return _results!
            }
        }
    }
    
    var history: UIBarButtonItem?
    let tableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        view.addSubview(tableView)
        tableView.register(MainMenuCell.self, forCellReuseIdentifier: "MainMenuCell")
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.easy.layout(Edges())
        title = "История"
        
        history = UIBarButtonItem(title: "Очистить", style: .done, target: self, action: #selector(clear))
        navigationItem.rightBarButtonItem = history
    }
    
    @objc func clear() {
        _results = nil
        CloudStorage.clear()
        tableView.reloadData()
    }
}
extension HistoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainMenuCell", for: indexPath) as! MainMenuCell
        cell.label.text = results[indexPath.row].result.sum.rubCopString
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let calcVC = CalcViewController()
        
        let r = results[indexPath.row]
        var items = [AnyObject]()
		let resultItem = r.result!
		resultItem.openPressed = {
			let vc = HintViewController()
			vc.text = formulaText
			UIApplication.shared.keyWindow?.rootViewController?.present(vc, animated: true, completion: nil)
		}
        items.append(resultItem)
        
        r.items.forEach {
            items.append(ResultItem(title: $0.key, desc: $0.value))
        }
        
        calcVC.items = items
        calcVC.title = "Результат"
        self.navigationController?.pushViewController(calcVC, animated: true)
    }
    
}
